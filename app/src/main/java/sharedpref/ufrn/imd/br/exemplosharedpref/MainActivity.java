package sharedpref.ufrn.imd.br.exemplosharedpref;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;


public class MainActivity extends Activity {

    private EditText login;
    private EditText senha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        login = (EditText) findViewById(R.id.loginText);
        senha = (EditText) findViewById(R.id.senhaText);

        SharedPreferences sharedPref = getPreferences(Activity.MODE_PRIVATE);
        login.setText(sharedPref.getString("login", ""));
        senha.setText(sharedPref.getString("senha", ""));
    }

    @Override
    protected void onStop() {
        super.onStop();

        if(login.getText() != null && senha.getText() != null) {
            SharedPreferences sharedPref = getPreferences(Activity.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString("login", login.getText().toString());
            editor.putString("senha", senha.getText().toString());
            editor.commit();
        }

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void logar(View view) {

        boolean erro = false;
        if(login.getText() == null || login.getText().toString().isEmpty()) {
            login.setError("Login: campo obrigat\u00f3rio.");
            erro = true;
        }
        if(senha.getText() == null || senha.getText().toString().isEmpty()) {
            senha.setError("Senha: campo obrigat\u00f3rio.");
            erro = true;
        }

        if(!erro) {
            if(login.getText().toString().equals("admin") && senha.getText().toString().equals("admin")) {
                Intent i = new Intent(this, MenuActivity.class);
                startActivity(i);
            } else {
                login.setError("Usu\u00e1rio n\u00e3o encontrado. Verifique seu login e senha!");
            }
        }


    }
}
